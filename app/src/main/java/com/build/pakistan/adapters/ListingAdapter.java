package com.build.pakistan.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.build.pakistan.R;
import com.build.pakistan.activity.product.ProductDetail;
import com.build.pakistan.base.BaseAdapter;
import com.build.pakistan.model.ListingModel;
import com.build.pakistan.utils.ContentSharing;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by arsalan.chishti on 8/9/2018.
 */

public class ListingAdapter extends BaseAdapter<ListingModel,ListingAdapter.ViewHolder> {
//https://proandroiddev.com/enter-animation-using-recyclerview-and-layoutanimation-part-1-list-75a874a5d213

    public ListingAdapter(Context context, List<ListingModel> li) {
        super(context,li);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, ListingModel item, int position) {
        ListingModel model = getItem(position);
        holder.titleTxt.setText(model.getName());
        Picasso.with(getContext()).load(model.getImage()).into(holder.img);
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getContext(), OfferDetail.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((Activity) getContext(),
                                holder.img,
                                ViewCompat.getTransitionName(holder.img));
                getContext().startActivity(intent, options.toBundle());*/

                Intent gotoSearch = new Intent(getContext(), ProductDetail.class);
               getContext().startActivity(gotoSearch);
            }
        });


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_product_listing, parent, false);

        return new ViewHolder(itemView);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout parent;
        TextView titleTxt;
        ImageView img;
        public ViewHolder(View itemView) {
            super(itemView);
            titleTxt = itemView.findViewById(R.id.company_txt);
            img = itemView.findViewById(R.id.image);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}
