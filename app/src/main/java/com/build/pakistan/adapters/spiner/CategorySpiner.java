package com.build.pakistan.adapters.spiner;

import android.widget.ArrayAdapter;

import com.build.pakistan.R;
import com.build.pakistan.model.dropdown.Category;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;
/**
 * Created by arsalan.chishti on 9/6/2018.
 */

public class CategorySpiner extends ArrayAdapter<Category> {

    private final LayoutInflater mInflater;
    private final Context mContext;
    private final List<Category> items;
    private final int mResource;

    public CategorySpiner(@NonNull Context context, @LayoutRes int resource,
                              @NonNull List objects) {
        super(context, resource, 0, objects);

        mContext = context;
        mInflater = LayoutInflater.from(context);
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView name = (TextView) view.findViewById(R.id.textView);


        Category offerData = items.get(position);

        name.setText(offerData.getName());

        return view;
    }
}
