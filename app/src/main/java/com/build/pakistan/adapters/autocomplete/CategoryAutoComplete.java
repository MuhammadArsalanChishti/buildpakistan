package com.build.pakistan.adapters.autocomplete;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.build.pakistan.R;
import com.build.pakistan.model.ListingModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arsalan.chishti on 9/6/2018.
 */

public class CategoryAutoComplete extends ArrayAdapter<ListingModel>{

    private Context context;
    private int resourceIdOut;
    private List<ListingModel> items, tempItems, suggestions;

    public CategoryAutoComplete(@NonNull Context context, int resource, ArrayList<ListingModel> items) {
        super(context, resource);
        this.items = items;
        this.context = context;
        this.resourceIdOut = resource;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                view = inflater.inflate(resourceIdOut, parent, false);
            }
            ListingModel fruit = getItem(position);
            TextView name = (TextView) view.findViewById(R.id.textView);
            TextView country = (TextView) view.findViewById(R.id.country);
            ImageView imageView = view.findViewById(R.id.imageView);
            ImageView verifiedView = view.findViewById(R.id.verifiedView);

            /*if (fruit.isVerified()) {
                verifiedView.setImageResource(R.drawable.valid);
            } else {
                verifiedView.setImageResource(R.drawable.bycot);
            }

            if (!fruit.getImageUrl().equals("")) {
                // Picasso.with(context).load(fruit.getImageUrl()).placeholder(R.drawable.holde).into(imageView);
            }
            Picasso.with(context).load(fruit.getImageUrl()).placeholder(R.drawable.holde).into(imageView);
            name.setText(fruit.getName());
            country.setText(fruit.getCountry());*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Nullable
    @Override
    public ListingModel getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return fruitFilter;
    }

    private Filter fruitFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            ListingModel fruit = (ListingModel) resultValue;
            return fruit.getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (ListingModel fruit : tempItems) {
                    if (fruit.getName().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(fruit);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<ListingModel> c = (ArrayList<ListingModel>) filterResults.values;
            if (filterResults != null && filterResults.count > 0) {
                clear();
                for (ListingModel cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}
