package com.build.pakistan.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.build.pakistan.R;
import com.build.pakistan.activity.product.ProductListing;
import com.build.pakistan.base.BaseAdapter;
import com.build.pakistan.model.Notification;

import java.util.List;

/**
 * Created by arsalan.chishti on 8/9/2018.
 */

public class NotificationListingAdapter extends BaseAdapter<Notification,NotificationListingAdapter.ViewHolder> {
//https://proandroiddev.com/enter-animation-using-recyclerview-and-layoutanimation-part-1-list-75a874a5d213

    public NotificationListingAdapter(Context context, List<Notification> li) {
        super(context,li);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, Notification item, int position) {
        Notification model = getItem(position);
        holder.titleTxt.setText(model.getTitle());
        //Picasso.with(getContext()).load(model.getImageDrawable()).into(holder.img);
        holder.img.setImageResource(model.getImageDrawable());

        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getContext(), OfferDetail.class);
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation((Activity) getContext(),
                                holder.img,
                                ViewCompat.getTransitionName(holder.img));
                getContext().startActivity(intent, options.toBundle());*/
                Intent intent = new Intent(getContext(), ProductListing.class);
                intent.putExtra("product",2);
                getContext().startActivity(intent);
            }
        });

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_notification_listing, parent, false);

        return new ViewHolder(itemView);
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout parent;
        TextView titleTxt,descTxt;
        ImageView img;
        public ViewHolder(View itemView) {
            super(itemView);
            titleTxt = itemView.findViewById(R.id.title);
            descTxt = itemView.findViewById(R.id.desc);
            img = itemView.findViewById(R.id.img);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}
