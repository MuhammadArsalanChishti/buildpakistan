package com.build.pakistan.application;

import android.app.Application;
import android.content.res.Configuration;


import com.build.pakistan.utils.LocaleUtils;

import java.util.Locale;

/**
 * Created by arsalan.chishti on 8/9/2018.
 */

public class MainApplication extends Application {

    public void onCreate(){
        super.onCreate();

        LocaleUtils.setLocale(new Locale("en"));
        LocaleUtils.updateConfig(this, getBaseContext().getResources().getConfiguration());
        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LocaleUtils.updateConfig(this, newConfig);
    }
}
