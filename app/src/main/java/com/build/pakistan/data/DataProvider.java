package com.build.pakistan.data;

import com.build.pakistan.R;
import com.build.pakistan.model.ListingModel;
import com.build.pakistan.model.Notification;
import com.build.pakistan.model.RelatedProducts;
import com.build.pakistan.model.SampleModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arsalan.chishti on 9/4/2018.
 */

public class DataProvider {

    private static int count = 16;
    private static String [] imagesData =
            {
            "https://i5.walmartimages.ca/images/Large/300/1_6/999999-78882103001_6.jpg",
            "https://pk.all.biz/img/pk/catalog/46568.jpeg",
            "http://gomart.pk/image/cache/data/incoming/img/p/1/8/7/7/dettol-surface-cleaner-floral-200ml-gomart-pakistan-1127-500x500.jpg",
            "http://electronics.qeemat.com/images/image-slider-3.jpg",
            "http://www.knorr.pk/Images/2796/2796-864655-CreamofChicken-Knorrwebsite-Products-480by270.jpg",
            "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ64EOXjQ5yH906Va_hm3ko-7-9ysgFR9YNzXRhyhfgDDP-YUcZ",
            "https://pk.daraz.io/ASfrP5RomQ8vrvEnkf2VWd0k5FI=/fit-in/500x500/filters:fill(white):sharpen(1,0,false):quality(80)/product/25/81947/1.jpg?7236",
                    "https://i5.walmartimages.ca/images/Large/300/1_6/999999-78882103001_6.jpg",
                    "https://pk.all.biz/img/pk/catalog/46568.jpeg",
                    "http://gomart.pk/image/cache/data/incoming/img/p/1/8/7/7/dettol-surface-cleaner-floral-200ml-gomart-pakistan-1127-500x500.jpg",
                    "http://electronics.qeemat.com/images/image-slider-3.jpg",
                    "http://www.knorr.pk/Images/2796/2796-864655-CreamofChicken-Knorrwebsite-Products-480by270.jpg",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ64EOXjQ5yH906Va_hm3ko-7-9ysgFR9YNzXRhyhfgDDP-YUcZ",
                    "https://pk.daraz.io/ASfrP5RomQ8vrvEnkf2VWd0k5FI=/fit-in/500x500/filters:fill(white):sharpen(1,0,false):quality(80)/product/25/81947/1.jpg?7236",
                    "http://electronics.qeemat.com/images/image-slider-3.jpg",
                    "http://www.knorr.pk/Images/2796/2796-864655-CreamofChicken-Knorrwebsite-Products-480by270.jpg"
    };
//
    //CATEGORIES DATA
    public static List<SampleModel> getCategoriesData(){

        List<SampleModel> dataList = new ArrayList<>();

        for(int i=0;i<count;i++){
            dataList.add(new SampleModel().setTitle("Name: "+i).setImageDrawable(R.drawable.drinks_icn));
        }
        return dataList;
    }


    //PRODUCTS DATA
    public static List<ListingModel> getProductsData(){

        List<ListingModel> dataList = new ArrayList<>();

        for(int i=0;i<count;i++){
            dataList.add(new ListingModel().setCategory("Category Name: "+i).setName("Name"+i).setCompany("Company"+i).setImage(imagesData[i]));
        }
        return dataList;
    }


    //NOTIFICATION DATA
    public static List<Notification> getNotificationData(){

        List<Notification> dataList = new ArrayList<>();
        Notification model = null;
        for(int i=0;i<count;i++){
            model = new Notification();
            model.setTitle("notification title"+i);
            model.setDesc("notification description"+i);
            model.setImageDrawable(R.drawable.drinks_icn);
            dataList.add(model);
        }
        return dataList;
    }

    //RELATED PRODUCTS DATA
    public static List<RelatedProducts> getRelatedProductsData(){

        List<RelatedProducts> dataList = new ArrayList<>();
        RelatedProducts model = null;
        for(int i=0;i<count;i++){
            model = new RelatedProducts();
            model.setName("product name"+i);
            model.setCompanyName("Company Name"+i);
            dataList.add(model);
        }
        return dataList;
    }
}
