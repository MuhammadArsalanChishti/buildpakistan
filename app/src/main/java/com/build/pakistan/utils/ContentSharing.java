package com.build.pakistan.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by arsalan.chishti on 8/26/2018.
 * Facebook - "com.facebook.katana"
 * Twitter - "com.twitter.android"
 * Instagram - "com.instagram.android"
 * Pinterest - "com.pinterest"
 * EXTRA UTILS
 * https://github.com/codepath/android_guides/wiki/Sharing-Content-with-Intents
 */

public class ContentSharing {

    public enum app {
        facebook, linkedin, twitter, whatsapp, insta
    }

    private final String wa = "com.whatsapp";
    private final String tw = "com.twitter.android";
    private final String fb = "com.facebook.katana";
    private final String li = "com.linkedin.android";
    private final String in = "com.instagram.android";

    private Intent intent;
    private Context mContext;
    private boolean isInsta = false;
    private String title, desc, image;


    public void shareTo(app appType) {

        Intent intent = null;
        if (appType.equals(app.facebook)) {
            intent = setIntent(fb);
            share(intent, fb);
        }
        if (appType.equals(app.linkedin)) {
            intent = setIntent(li);
            share(intent, li);
        }
        if (appType.equals(app.twitter)) {
            intent = setIntent(tw);
            share(intent, tw);
        }
        if (appType.equals(app.whatsapp)) {
            intent = setIntent(wa);
            share(intent, wa);
        }
        if (appType.equals(app.insta)) {
            isInsta = true;
            intent = setIntent(in);
            share(intent, in);
        }


    }

    private Intent setIntent(String app) {
        intent = getContext().getPackageManager().getLaunchIntentForPackage(app);
        return intent;
    }

    private void share(Intent intent, String type) {
        if (intent != null) {
            Intent shareIntent = new Intent();
            if (isInsta) {
                shareIntent.setType("image/*");
                if(getImage()!=null){
                    Uri file = Uri.parse(getImage());
                    shareIntent.putExtra(Intent.EXTRA_STREAM, file);
                    isInsta = false;
                }

            } else {
                shareIntent.setType("text/plain");
            }

            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.setPackage(type);
            shareIntent.putExtra(Intent.EXTRA_TITLE, getTitle());
            shareIntent.putExtra(Intent.EXTRA_TEXT, getDesc());
            // Start the specific social application
            getContext().startActivity(shareIntent);
        } else {
            Toast.makeText(getContext(), "app not installed", Toast.LENGTH_SHORT).show();
        }
    }
    public void shareToAll( String body,String subject) {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");

        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,subject);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, body);
        getContext().startActivity(Intent.createChooser(sharingIntent, "Shearing Option"));
    }


    /*getters and setters*/

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
