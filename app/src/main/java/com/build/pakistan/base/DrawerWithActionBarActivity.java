package com.build.pakistan.base;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.build.pakistan.R;
import com.build.pakistan.activity.MainActivity;
import com.build.pakistan.activity.barcode.BarcodeActivity;
import com.build.pakistan.activity.bookmark.BookmarkListing;
import com.build.pakistan.activity.category.CategoryListing;
import com.build.pakistan.activity.dashboard.DashBoard;
import com.build.pakistan.activity.notification.Notifications;
import com.build.pakistan.activity.product.AddProduct;
import com.build.pakistan.activity.product.ProductListing;
import com.build.pakistan.activity.contact.ContactUs;
import com.build.pakistan.activity.setting.Setting;
import com.build.pakistan.activity.user.EditProfile;
import com.build.pakistan.widgets.CustomTypefaceSpan;

/**
 * Created by arsalan.chishti on 9/3/2018.
 */

public class DrawerWithActionBarActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    private DrawerLayout drawer;
    private RelativeLayout actionBar;
    private ImageView backBtn, drawerBtn, crossBtn;
    private TextView titleText;
    private NavigationView navigationView;

    protected void setNavifationFont() {
        navigationView = findViewById(R.id.nav_view);

        if (navigationView != null) {
            crossBtn = navigationView.findViewById(R.id.cross_btn);
         /*   LinearLayout home = navigationView.findViewById(R.id.home_menu);


            home.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(DrawerWithActionBarActivity.this, "home item", Toast.LENGTH_SHORT).show();
                }
            });
*/

         onDrawerItemClick(navigationView);

            crossBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (drawer != null) {
                        drawer.closeDrawer(Gravity.START);
                    }

                }
            });
        }
       /* if (navigationView != null) {
            Menu m = navigationView.getMenu();

            Typeface tf1 = Typeface.createFromAsset(getAssets(), "fonts/WorkSans-Regular.ttf");

            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);
                SpannableString s = new SpannableString(mi.getTitle());
                s.setSpan(new CustomTypefaceSpan("", tf1), 0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                mi.setTitle(s);
            }
        }*/
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onStart() {
        super.onStart();
        setNavifationFont();
    }

    protected RecyclerView findRecyclerView(int recyclerID) {

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_anim);
        RecyclerView recyclerView = findViewById(recyclerID);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);
        return recyclerView;

    }

    protected RecyclerView findGridRecyclerView(int recyclerID) {

        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_anim);
        RecyclerView recyclerView = findViewById(recyclerID);
        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 2);
        //recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);
        return recyclerView;

    }

    protected DrawerWithActionBarActivity activateToolBar() {

        if (actionBar == null) {
            actionBar = findViewById(R.id.action_bar_container);

            backBtn = findViewById(R.id.back_btn);
            drawerBtn = findViewById(R.id.drawer_btn);
            titleText = findViewById(R.id.title_txt);
        }
        return this;
    }

    public void showBackActionBar(String title) {

        titleText.setText(title);
        drawerBtn.setVisibility(View.GONE);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public DrawerWithActionBarActivity showDrawerActionBar(String title) {
        titleText.setText(title);
        backBtn.setVisibility(View.GONE);
        drawerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(DrawerWithActionBarActivity.this, "Drawer Btn click", Toast.LENGTH_SHORT).show();
                activateDrawer();

                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }


            }
        });
        return this;
    }

    public void showTitle(String title) {
        titleText.setText(title);
        backBtn.setVisibility(View.GONE);
        drawerBtn.setVisibility(View.GONE);
    }

    public void activateDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.item_category) {
            // Handle the camera action
        } else if (id == R.id.item_products) {



        } else if (id == R.id.item_bookmark) {

        } else if (id == R.id.item_scan) {

        } else if (id == R.id.item_setting) {

        } else if (id == R.id.item_contactus) {

        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onDrawerItemClick(NavigationView navView) {

        String tag = (String) navView.getTag();

       // LinearLayout itemLayout = navView.findViewById(R.id.list_items);

        RelativeLayout userEdit = navView.findViewById(R.id.user_edit_section);
        userEdit.setOnClickListener(this);

        LinearLayout home = navView.findViewById(R.id.home_menu);
        home.setOnClickListener(this);
        
        LinearLayout category = navView.findViewById(R.id.category_menu);
        category.setOnClickListener(this);
        
        LinearLayout scan = navView.findViewById(R.id.scan_menu);
        scan.setOnClickListener(this);
        
        LinearLayout products = navView.findViewById(R.id.products_menu);
        products.setOnClickListener(this);
        
        LinearLayout addProducts = navView.findViewById(R.id.add_product_menu);
        addProducts.setOnClickListener(this);
        
        LinearLayout favorites = navView.findViewById(R.id.favorites_menu);
        favorites.setOnClickListener(this);
        
        LinearLayout notification = navView.findViewById(R.id.notification_menu);
        notification.setOnClickListener(this);
        
        LinearLayout settings = navView.findViewById(R.id.settings_menu);
        settings.setOnClickListener(this);
        
        LinearLayout contact = navView.findViewById(R.id.contact_menu);
        contact.setOnClickListener(this);
        
        LinearLayout logout = navView.findViewById(R.id.logout_menu);
        logout.setOnClickListener(this);
        

        
    }


    @Override
    public void onClick(final View v) {
        //drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                //Execute code here
                switch (v.getId()){

                    case R.id.home_menu:{
                        if (DashBoard.class != null) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, DashBoard.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }

                    case R.id.category_menu:{

                        if (!(DrawerWithActionBarActivity.this instanceof CategoryListing)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, CategoryListing.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }

                    case R.id.scan_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof BarcodeActivity)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, BarcodeActivity.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }

                    case R.id.products_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof ProductListing)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, ProductListing.class);
                            gotoPro.putExtra("product", 1);
                            startActivity(gotoPro);
                        }
                        break;
                    }

                    case R.id.add_product_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof AddProduct)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, AddProduct.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }

                    case R.id.favorites_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof BookmarkListing)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, BookmarkListing.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }

                    case R.id.notification_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof Notifications)) {
                            Intent gotoNotifi = new Intent(DrawerWithActionBarActivity.this, Notifications.class);
                            startActivity(gotoNotifi);
                        }
                        break;
                    }
                    case R.id.settings_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof Setting)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, Setting.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }
                    case R.id.contact_menu:{
                        if (!(DrawerWithActionBarActivity.this instanceof ContactUs)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, ContactUs.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }
                    case R.id.logout_menu:{
                        Toast.makeText(DrawerWithActionBarActivity.this, "Logout", Toast.LENGTH_SHORT).show();
                        break;
                    }

                    case R.id.user_edit_section:{
                        if (!(DrawerWithActionBarActivity.this instanceof EditProfile)) {
                            Intent gotoPro = new Intent(DrawerWithActionBarActivity.this, EditProfile.class);
                            startActivity(gotoPro);
                        }
                        break;
                    }



                }

            }
        }, 200);





    }
    
    /*
    * if (tag.equals("home")) {
            
        }
        if (tag.equals("category")) {
            if (!(this instanceof CategoryListing)) {
                Intent gotoCat = new Intent(this, CategoryListing.class);
                startActivity(gotoCat);
            }

        }
        if (tag.equals("scan")) {
            if (!(this instanceof BarcodeActivity)) {
                Intent gotoPro = new Intent(this, BarcodeActivity.class);
                startActivity(gotoPro);
            }
        }
        if (tag.equals("products")) {
            if (!(this instanceof ProductListing)) {
                Intent gotoPro = new Intent(this, ProductListing.class);
                gotoPro.putExtra("product", 1);
                startActivity(gotoPro);
            }
        }
        if (tag.equals("add_products")) {
            if (!(this instanceof BookmarkListing)) {
                Intent gotoPro = new Intent(this, BookmarkListing.class);
                startActivity(gotoPro);
            }

        }
        if (tag.equals("favorites")) {
            if (!(this instanceof BookmarkListing)) {
                Intent gotoPro = new Intent(this, BookmarkListing.class);
                startActivity(gotoPro);
            }
        }
        if (tag.equals("notification")) {

        }
        if (tag.equals("settings")) {
            if (!(this instanceof Setting)) {
                Intent gotoPro = new Intent(this, Setting.class);
                startActivity(gotoPro);
            }
        }
        if (tag.equals("contact")) {
            if (!(this instanceof ContactUs)) {
                Intent gotoPro = new Intent(this, ContactUs.class);
                startActivity(gotoPro);
            }

        }
        if (tag.equals("logout")) {
        }*/
}
