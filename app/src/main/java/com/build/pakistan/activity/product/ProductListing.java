package com.build.pakistan.activity.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.build.pakistan.R;
import com.build.pakistan.activity.search.GloabalSearch;
import com.build.pakistan.adapters.ListingAdapter;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.data.DataProvider;
import com.build.pakistan.model.ListingModel;

import java.util.ArrayList;
import java.util.List;

public class ProductListing extends DrawerWithActionBarActivity {

    RecyclerView recyclerView;
    RelativeLayout containerSearch;
    private int fromMenu = 1;

    private int fromCategory= 2;
    private Spinner dropDownContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);
        int key = getIntent().getIntExtra("product",1);
        activateToolBar().showDrawerActionBar(getResources().getString(R.string.prod_listing));

        dropDownContainer = findViewById(R.id.spiner_search);
        containerSearch = findViewById(R.id.search);
        recyclerView = findRecyclerView(R.id.recycler);


        if(key==fromCategory){
            containerSearch.setVisibility(View.GONE);
            dropDownContainer.setVisibility(View.VISIBLE);

            List<String> list = new ArrayList<String>();
            list.add("Select Category");
            list.add("Food");
            list.add("Toys");
            list.add("Electronics");
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            dropDownContainer.setAdapter(dataAdapter);
        }


        containerSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gotoSearch = new Intent(ProductListing.this, GloabalSearch.class);
                startActivity(gotoSearch);
            }
        });


        ListingAdapter adapter = new ListingAdapter(this, DataProvider.getProductsData());
        recyclerView.setAdapter(adapter);
    }
}
