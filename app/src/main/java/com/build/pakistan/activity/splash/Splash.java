package com.build.pakistan.activity.splash;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.build.pakistan.R;
import com.build.pakistan.activity.dashboard.DashBoard;
import com.build.pakistan.base.AppActivity;
import com.build.pakistan.utils.SessionManager;

public class Splash extends AppActivity {

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sessionManager = new SessionManager(this);
        String  lan =sessionManager.getStringValue("lan");

        if(lan.equals("")){
            sessionManager.setStringValue("lan","en");
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent gotoCategory = new Intent(Splash.this, DashBoard.class);
                startActivity(gotoCategory);
                finish();
            }
        },1500);
    }
}
