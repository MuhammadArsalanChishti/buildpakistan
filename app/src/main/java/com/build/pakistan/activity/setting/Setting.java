package com.build.pakistan.activity.setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.build.pakistan.R;
import com.build.pakistan.activity.splash.Splash;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.utils.LocaleUtils;
import com.build.pakistan.utils.SessionManager;
import com.build.pakistan.widgets.BPTextview;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Setting extends DrawerWithActionBarActivity {

    private BPTextview title;
    private Spinner languageSpnr;
    private boolean firstTime=false;
    private int first=0;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        inti();
        setLanguageSpnr();
    }



    private void inti(){
        activateToolBar().showBackActionBar("Settings");
        sessionManager =  new SessionManager(this);

        title = findViewById(R.id.lan_title);
        languageSpnr = findViewById(R.id.language);
    }

    private void setLanguageSpnr(){List<String> list = new ArrayList<String>();
        list.add(0,"Change Language");
        list.add(1,"English");
        list.add(2,"Urdu");

        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        languageSpnr.setAdapter(dataAdapter2);



        if(sessionManager.getStringValue("lan").equals("ur")){
            title.setText(getResources().getString(R.string.urdu));
        }
        else {
            title.setText(getResources().getString(R.string.eng));
        }


        languageSpnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(first>=1){
                    int lan = (int) parent.getSelectedItemId();
                    if(lan==1){

                        if(sessionManager.getStringValue("lan").equals("en")){
                            Toast.makeText(Setting.this, getResources().getString(R.string.english_selected), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        //english clicked
                        LocaleUtils.setLocale(new Locale("en"));
                        sessionManager.setStringValue("lan","en");
                        finish();
                        //startActivity(new Intent(Setting.this, Splash.class));
                        finishAffinity();

                        Intent intent = new Intent(getApplicationContext(), Splash.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                    }
                    else if(lan==2) {

                        if(sessionManager.getStringValue("lan").equals("ur")){
                            Toast.makeText(Setting.this, getResources().getString(R.string.urdu_selected), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        //urdu clicked
                        LocaleUtils.setLocale(new Locale("ur"));
                        sessionManager.setStringValue("lan","ur");
                        finish();
                        finishAffinity();
                        //startActivity(new Intent(Setting.this, Splash.class));

                        Intent intent = new Intent(getApplicationContext(), Splash.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                }

                //firstTime=true;
                first++;

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });}

}
