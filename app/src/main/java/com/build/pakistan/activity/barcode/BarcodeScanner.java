package com.build.pakistan.activity.barcode;

import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.widget.Toast;

import com.build.pakistan.R;
import com.build.pakistan.activity.product.ProductListing;
import com.build.pakistan.base.AppActivity;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class BarcodeScanner extends AppActivity implements BarcodeReader.BarcodeReaderListener{


    private BarcodeReader barcodeReader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scanner);
        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
    }

    @Override
    public void onScanned(com.google.android.gms.vision.barcode.Barcode barcode) {
        barcodeReader.playBeep();

        // playing barcode reader beep sound


        // ticket details activity by passing barcode
        Intent intent = new Intent(BarcodeScanner.this, ProductListing.class);
        intent.putExtra("code", barcode.displayValue);
        startActivity(intent);
    }

    @Override
    public void onScannedMultiple(List<com.google.android.gms.vision.barcode.Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<com.google.android.gms.vision.barcode.Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
        Toast.makeText(getApplicationContext(), "Error occurred while scanning " + errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraPermissionDenied() {
        finish();
    }
}
