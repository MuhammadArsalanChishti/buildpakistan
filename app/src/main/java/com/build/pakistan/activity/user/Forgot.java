package com.build.pakistan.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.build.pakistan.R;
import com.build.pakistan.base.BaseActivity;
import com.build.pakistan.base.DrawerWithActionBarActivity;

public class Forgot extends DrawerWithActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        activateToolBar().showBackActionBar(getResources().getString(R.string.forgot_pass));
    }

    public void forgot(View v){

        switch (v.getId()){

            case R.id.forgot_btn:
                Intent gotoReset = new Intent(this,ResetPassword.class);
                startActivity(gotoReset);
                break;
        }
    }
}
