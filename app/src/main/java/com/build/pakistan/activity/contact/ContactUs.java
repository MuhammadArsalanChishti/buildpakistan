package com.build.pakistan.activity.contact;

import android.os.Bundle;

import com.build.pakistan.R;
import com.build.pakistan.base.DrawerWithActionBarActivity;

public class ContactUs extends DrawerWithActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        activateToolBar().showBackActionBar("ContactUs");
    }
}
