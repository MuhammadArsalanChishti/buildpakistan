package com.build.pakistan.activity.user;

import android.os.Bundle;

import com.build.pakistan.R;
import com.build.pakistan.base.DrawerWithActionBarActivity;

public class ResetPassword extends DrawerWithActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        activateToolBar().showBackActionBar(getResources().getString(R.string.reset_pass));
    }
}
