package com.build.pakistan.activity.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.build.pakistan.activity.barcode.BarcodeActivity;
import com.build.pakistan.activity.bookmark.BookmarkListing;
import com.build.pakistan.activity.category.CategoryListing;
import com.build.pakistan.activity.contact.ContactUs;
import com.build.pakistan.activity.product.AddProduct;
import com.build.pakistan.activity.product.ProductListing;
import com.build.pakistan.activity.search.GloabalSearch;
import com.build.pakistan.activity.setting.Setting;
import com.build.pakistan.activity.user.Login;
import com.build.pakistan.utils.LocaleUtils;
import com.build.pakistan.widgets.HomeGridMenuButton;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.build.pakistan.R;

public class DashBoard extends YouTubeBaseActivity implements View.OnClickListener{

    private HomeGridMenuButton catBtn,proBtn,adProBtn,barCode,bookMark,searchBtn,settingBtn,contactBtn,loginBtn;
    private TextView next;
    private  YouTubePlayerView youTubePlayerView;
    public DashBoard() {
        LocaleUtils.updateConfig(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);

        init();

         youTubePlayerView = (YouTubePlayerView) findViewById(R.id.player);

        youTubePlayerView.initialize("AIzaSyBa5k_lzmBWE8qcYLIkP8MsG3bMbKNZSu4",
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {

                        // do any work here to cue video, play video, etc.
                        youTubePlayer.cueVideo("5xVh-7ywKpE");
                        //youTubePlayer.cueVideo("pFwrUkMoAFk");
                        //https://youtu.be/pFwrUkMoAFk
                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {
                        Toast.makeText(DashBoard.this, "onInitializationFailure", Toast.LENGTH_SHORT).show();

                    }
                });
    }


    private void init(){
/*
        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/

        catBtn = findViewById(R.id.categories_btn);
        catBtn.setOnClickListener(this);

        proBtn = findViewById(R.id.products_btn);
        proBtn.setOnClickListener(this);

        adProBtn = findViewById(R.id.add_products_btn);
        adProBtn.setOnClickListener(this);

        barCode = findViewById(R.id.scan_btn);
        barCode.setOnClickListener(this);

        bookMark = findViewById(R.id.bookmark_btn);
        bookMark.setOnClickListener(this);


        searchBtn = findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(this);

        settingBtn = findViewById(R.id.setting_btn);
        settingBtn.setOnClickListener(this);

        contactBtn = findViewById(R.id.contact_btn);
        contactBtn.setOnClickListener(this);

        loginBtn = findViewById(R.id.login_btn);
        loginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.categories_btn:
                gotoNext(CategoryListing.class);
                break;

            case R.id.products_btn:
                //gotoNext(ProductListing.class);
                Intent gotoProListing = new Intent(this,ProductListing.class);
                gotoProListing.putExtra("product",1);
                startActivity(gotoProListing);
                break;

            case R.id.add_products_btn:
                gotoNext(AddProduct.class);
                break;

            case R.id.scan_btn:
                gotoNext(BarcodeActivity.class);
                break;

            case R.id.bookmark_btn:
                gotoNext(BookmarkListing.class);
                break;

            case R.id.search_btn:
                gotoNext(GloabalSearch.class);
                break;

            case R.id.setting_btn:
                gotoNext(Setting.class);
                break;

            case R.id.contact_btn:
                gotoNext(ContactUs.class);
                break;

            case R.id.login_btn:
                gotoNext(Login.class);
                break;

        }
    }

    private void gotoNext(Class<?> activity){
        Intent gotoNext = new Intent(this,activity);
        startActivity(gotoNext);
    }
}
