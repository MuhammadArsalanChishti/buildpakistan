package com.build.pakistan.activity.user;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.build.pakistan.R;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }
    public void signup(View v){

        switch (v.getId()){

            case R.id.forgot:
                Intent gotoForgot = new Intent(this,Forgot.class);
                startActivity(gotoForgot);
                break;

            case R.id.register:
                Intent gotoSign = new Intent(this,Register.class);
                startActivity(gotoSign);
                break;

        }

    }
}
