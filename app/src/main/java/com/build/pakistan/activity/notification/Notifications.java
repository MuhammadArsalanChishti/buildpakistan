package com.build.pakistan.activity.notification;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.build.pakistan.R;
import com.build.pakistan.adapters.NotificationListingAdapter;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.data.DataProvider;
import com.build.pakistan.model.Notification;

import java.util.ArrayList;
import java.util.List;


public class Notifications extends DrawerWithActionBarActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        activateToolBar().showBackActionBar(getResources().getString(R.string.notification));

        recyclerView = findRecyclerView(R.id.recycler);
        NotificationListingAdapter adapter = new NotificationListingAdapter(this, DataProvider.getNotificationData());
        recyclerView.setAdapter(adapter);


    }
}
