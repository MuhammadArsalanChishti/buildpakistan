package com.build.pakistan.activity.barcode;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.build.pakistan.R;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.widgets.BPTextview;

public class BarcodeActivity extends DrawerWithActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);

        activateToolBar().showBackActionBar(getResources().getString(R.string.scan_code));

        BPTextview scanButton = findViewById(R.id.code_scan);
        scanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent barcodeScanner = new Intent(BarcodeActivity.this,BarcodeScanner.class);
                startActivity(barcodeScanner);
                finish();
            }
        });
    }


}
