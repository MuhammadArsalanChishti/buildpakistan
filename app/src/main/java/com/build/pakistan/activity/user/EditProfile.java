package com.build.pakistan.activity.user;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.build.pakistan.R;
import com.build.pakistan.base.DrawerWithActionBarActivity;

public class EditProfile extends DrawerWithActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        activateToolBar().showBackActionBar(getResources().getString(R.string.edit_profile));
    }
}
