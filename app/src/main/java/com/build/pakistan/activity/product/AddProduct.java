package com.build.pakistan.activity.product;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.build.pakistan.R;
import com.build.pakistan.adapters.autocomplete.CountryAutoComplete;
import com.build.pakistan.adapters.spiner.CategorySpiner;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.model.dropdown.Category;
import com.build.pakistan.model.dropdown.Country;
import com.build.pakistan.widgets.BPTextview;

import java.util.ArrayList;
import java.util.List;

public class AddProduct extends DrawerWithActionBarActivity {
    private Spinner dropDownContainer,getDropDownContainer2;
    private AppCompatAutoCompleteTextView autoTextViewCustom;
    private CountryAutoComplete countryAdapter;
    private ArrayList<Country> countrytArrayList;
    private BPTextview imagePicker;
    private ImageView imag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        activateToolBar().showBackActionBar(getResources().getString(R.string.add_product));

        imagePicker = findViewById(R.id.image_picker);
        imag = findViewById(R.id.selected_image);
        dropDownContainer=  findViewById(R.id.category_txt);

        getDropDownContainer2=  findViewById(R.id.categoty_spnr);

        autoTextViewCustom = (AppCompatAutoCompleteTextView) findViewById(R.id.country_txt);
        autoTextViewCustom.setThreshold(2);

        List<Category> list = new ArrayList<>();
        list.add(new Category("Select Category"));
        list.add(new Category("Foods"));
        list.add(new Category("Toys"));
        list.add(new Category("Electronics"));

        CategorySpiner spnr = new CategorySpiner(this,R.layout.item_row,list);
        dropDownContainer.setAdapter(spnr);


        List<Category> list2 = new ArrayList<>();

        list2.add(new Category("Select Sub Category"));
        list2.add(new Category("sub Foods"));
        list2.add(new Category("sub Toys"));
        list2.add(new Category("sub Electronics"));


  /*      ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list2);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        getDropDownContainer2.setAdapter(dataAdapter2);*/

        CategorySpiner spnr2 = new CategorySpiner(this,R.layout.item_row,list2);
        getDropDownContainer2.setAdapter(spnr2);

        countrytArrayList = new ArrayList<>();

        countrytArrayList.add(new Country("pakistan"));
        countrytArrayList.add(new Country("germany"));
        countrytArrayList.add(new Country("canada"));
        countrytArrayList.add(new Country("china"));
        countrytArrayList.add(new Country("india"));
        //countrytArrayList.add(new Country("pakistan"));

        countryAdapter = new CountryAutoComplete(this, R.layout.item_row, countrytArrayList);

        autoTextViewCustom.setAdapter(countryAdapter);
        autoTextViewCustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Country product = (Country) adapterView.getItemAtPosition(i);

                Toast.makeText(AddProduct.this, ""+product.getName(), Toast.LENGTH_SHORT).show();
            }
        });


        imagePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto , 1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        switch(requestCode) {
            case 1:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    imag.setImageURI(selectedImage);
                }
                break;
        }
    }
}
