package com.build.pakistan.activity.bookmark;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.build.pakistan.R;
import com.build.pakistan.adapters.ListingAdapter;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.data.DataProvider;

import java.util.ArrayList;
import java.util.List;

public class BookmarkListing extends DrawerWithActionBarActivity {
    private RecyclerView recyclerView;
    private Spinner spinerSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookmark_listing);
        activateToolBar().showBackActionBar(getResources().getString(R.string.bookmarks));
        spinerSearch = findViewById(R.id.spiner_search);

        List<String> list = new ArrayList<String>();
        list.add("Select Category");
        list.add("Food");
        list.add("Toys");
        list.add("Electronics");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinerSearch.setAdapter(dataAdapter);


        recyclerView = findRecyclerView(R.id.recycler);
        ListingAdapter adapter = new ListingAdapter(this, DataProvider.getProductsData());
        recyclerView.setAdapter(adapter);
    }
}
