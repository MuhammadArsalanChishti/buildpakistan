package com.build.pakistan.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.build.pakistan.R;
import com.build.pakistan.activity.barcode.BarcodeScanner;
import com.build.pakistan.activity.category.CategoryListing;
import com.build.pakistan.activity.contact.ContactUs;
import com.build.pakistan.activity.product.ProductDetail;
import com.build.pakistan.activity.product.ProductListing;
import com.build.pakistan.activity.search.GloabalSearch;
import com.build.pakistan.activity.setting.Setting;
import com.build.pakistan.activity.splash.Splash;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1 -Category  (image, name)
        // 2 -ProductListing   (top bar, heading, filter, name, company, category)
        // 3 -Setting   (notification, change language)
        // 4 -Contact us
        // 5 -User listing (bookmark listing)
        // 6 -Code reading (reading bar code , search product by name)
        // 7 -GlobalFilter (name, category, country)
        // 8 -left menu
    }

    public void category(View v) {
        switch (v.getId()) {
            case R.id.catagory_btn:
                Intent gotoCate = new Intent(this, CategoryListing.class);
                startActivity(gotoCate);
                break;

            case R.id.listing_btn:
                Intent gotoListing = new Intent(this, ProductListing.class);
                startActivity(gotoListing);
                break;

            case R.id.search_btn:
                Intent gotoSearch = new Intent(this, GloabalSearch.class);
                startActivity(gotoSearch);
                break;

            case R.id.setting_btn:
                Intent gotoSetting = new Intent(this, Setting.class);
                startActivity(gotoSetting);
                break;

            case R.id.contact_btn:
                Intent gotoContact = new Intent(this, ContactUs.class);
                startActivity(gotoContact);
                break;

            case R.id.code_search_btn:
                Intent gotoCodeReader = new Intent(this, BarcodeScanner.class);
                startActivity(gotoCodeReader);
                break;

            case R.id.product_detail:
                Intent gotoDetail = new Intent(this, ProductDetail.class);
                startActivity(gotoDetail);
                break;


            case R.id.splash_btn:
                Intent gotoSp= new Intent(this, Splash.class);
                startActivity(gotoSp);
                break;
        }

    }
}
