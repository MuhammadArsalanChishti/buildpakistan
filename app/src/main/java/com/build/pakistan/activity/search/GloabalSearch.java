package com.build.pakistan.activity.search;

import android.os.Bundle;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.build.pakistan.R;
import com.build.pakistan.adapters.autocomplete.CountryAutoComplete;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.model.dropdown.Country;

import java.util.ArrayList;
import java.util.List;

public class GloabalSearch extends DrawerWithActionBarActivity {

    private AppCompatAutoCompleteTextView autoTextViewCustom;
    private CountryAutoComplete countryAdapter;
    private ArrayList<Country> countrytArrayList;
    private Spinner dropDownContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        activateToolBar().showBackActionBar("Search");
        autoTextViewCustom = (AppCompatAutoCompleteTextView) findViewById(R.id.country_txt);
        dropDownContainer=  findViewById(R.id.category_txt);
        autoTextViewCustom.setThreshold(2);
        countrytArrayList = new ArrayList<>();

        countrytArrayList.add(new Country("pakistan"));
        countrytArrayList.add(new Country("germany"));
        countrytArrayList.add(new Country("canada"));
        countrytArrayList.add(new Country("china"));
        countrytArrayList.add(new Country("india"));
        countrytArrayList.add(new Country("pakistan"));

        countryAdapter = new CountryAutoComplete(this, R.layout.item_row, countrytArrayList);

        autoTextViewCustom.setAdapter(countryAdapter);
        autoTextViewCustom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Country product = (Country) adapterView.getItemAtPosition(i);

                Toast.makeText(GloabalSearch.this, ""+product.getName(), Toast.LENGTH_SHORT).show();
            }
        });


        List<String> list = new ArrayList<String>();
        list.add("Select Category");
        list.add("Food");
        list.add("Toys");
        list.add("Electronics");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropDownContainer.setAdapter(dataAdapter);
    }
}
