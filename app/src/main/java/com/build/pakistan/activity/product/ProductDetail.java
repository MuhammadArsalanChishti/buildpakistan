package com.build.pakistan.activity.product;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.build.pakistan.R;
import com.build.pakistan.adapters.CategoryListingAdapter;
import com.build.pakistan.adapters.RelatedProductsAdapter;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.data.DataProvider;
import com.build.pakistan.model.SampleModel;

import java.util.ArrayList;
import java.util.List;

public class ProductDetail extends DrawerWithActionBarActivity {
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail_parallex);
        //activateToolBar().showBackActionBar("Product Detail");
        recyclerView = findRecyclerView(R.id.recycler);


        RelatedProductsAdapter sampleAdptr = new RelatedProductsAdapter(this, DataProvider.getRelatedProductsData());
        recyclerView.setAdapter(sampleAdptr);
    }
}
