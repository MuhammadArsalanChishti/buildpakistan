package com.build.pakistan.activity.user;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.build.pakistan.R;
import com.build.pakistan.base.DrawerWithActionBarActivity;

public class Register extends DrawerWithActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        activateToolBar().showBackActionBar(getResources().getString(R.string.signup));
    }
}
