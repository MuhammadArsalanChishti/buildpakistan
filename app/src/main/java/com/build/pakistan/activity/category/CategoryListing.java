package com.build.pakistan.activity.category;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.build.pakistan.R;
import com.build.pakistan.adapters.CategoryListingAdapter;
import com.build.pakistan.base.DrawerWithActionBarActivity;
import com.build.pakistan.data.DataProvider;
import com.build.pakistan.model.SampleModel;

import java.util.ArrayList;
import java.util.List;

public class CategoryListing extends DrawerWithActionBarActivity {

    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_listing);

        activateToolBar().showDrawerActionBar("Category Listing");
        CategoryListingAdapter sampleAdptr = new CategoryListingAdapter(this, DataProvider.getCategoriesData());
        recyclerView = findGridRecyclerView(R.id.recycler);

        recyclerView.setAdapter(sampleAdptr);

        //setNavifationFont();
    }
}
