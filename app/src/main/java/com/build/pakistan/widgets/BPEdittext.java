package com.build.pakistan.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;

import com.build.pakistan.R;


/**
 * Created by arsalan.chishti on 8/7/2018.
 */

public class BPEdittext extends android.support.v7.widget.AppCompatEditText {

    public BPEdittext(Context context) {
        super(context);
        if(!isInEditMode())
        init(null);
    }

    public BPEdittext(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if(!isInEditMode())
        init(attrs);
    }

    public BPEdittext(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if(!isInEditMode())
        init(attrs);
    }

    private void init(AttributeSet attrs){
        if(attrs!=null){
            TypedArray arry =getContext().obtainStyledAttributes(attrs, R.styleable.BPTextview);
            String mFontName = arry.getString(R.styleable.BPTextview_font_name);
            if(mFontName!=null){
                Log.e("** font",mFontName);
                Typeface mTypeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/"+mFontName);
                setTypeface(mTypeface);
            }
            else {
                mFontName="sfui_medium.ttf";
                Typeface mTypeface = Typeface.createFromAsset(getContext().getAssets(),"fonts/"+mFontName);
                setTypeface(mTypeface);
            }
            arry.recycle();

        }
    }
}
