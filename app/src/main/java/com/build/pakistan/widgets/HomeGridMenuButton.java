package com.build.pakistan.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.build.pakistan.R;


/**
 * Created by owais.ali on 3/5/2017.
 */

public class HomeGridMenuButton extends RelativeLayout {

    private Context context;
    private Theme theme;

    private enum Theme{
        LIGHT, DARK
    }
    private int normalColor;
    private int hoverColor;
    private int activeColor;
    private boolean isDisable;

    public HomeGridMenuButton(Context context) {
        this(context, null);
    }

    public HomeGridMenuButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HomeGridMenuButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;
        final TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.HomeGridMenuButton);
        try{
            final int btnStyle = array.getInt(R.styleable.HomeGridMenuButton_button_style, 0);
            theme = btnStyle == 1 ? Theme.DARK : Theme.LIGHT;
            setupColors();
        }
        finally {
            array.recycle();
        }
        setupColors();
        setClickable(true);
        setWillNotDraw(false);
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                final int width = getWidth();
                int height = width;
               // if(getId() == R.id.alt_btn_facilities || getId() == R.id.alt_btn_profile || getId() == R.id.alt_btn_news || getId() == R.id.alt_btn_gallery) height += 1;
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, height);
                setLayoutParams(layoutParams);
                requestLayout();
                getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawColor(activeColor);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(isDisable) return false;

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:
                activeColor = hoverColor;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                activeColor = normalColor;
                break;
        }

        invalidate();
        return super.onTouchEvent(event);
    }

    private void setupColors(){

        // light theme
        if(Theme.LIGHT.equals(theme)){
            normalColor = ContextCompat.getColor(context, R.color.color_1f4f55);
            hoverColor  = ContextCompat.getColor(context, R.color.color_183f44);
        }

        // dark theme
        else{
            normalColor = ContextCompat.getColor(context, R.color.color_19565e);
            hoverColor  = ContextCompat.getColor(context, R.color.color_08373d);
        }

        activeColor = normalColor;
    }

    public void disable(){
        isDisable = true;
        setClickable(false);
        //activeColor = Color.parseColor("#55546b");
        //((ColorImageView) findViewWithTag("ic")).setColor(Color.parseColor("#616076"));
        //((MBSCTextView) findViewWithTag("name")).setTextColor(Color.parseColor("#616076"));
        //((ImageView) findViewWithTag("ic")).setColor(Color.parseColor("#50FFFFFF"));
        ((BPTextview) findViewWithTag("name")).setTextColor(Color.parseColor("#50FFFFFF"));

        invalidate();
    }

}
