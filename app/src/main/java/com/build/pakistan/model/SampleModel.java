package com.build.pakistan.model;

/**
 * Created by arsalan.chishti on 9/3/2018.
 */

public class SampleModel {

    private String title;
    private String image;
    private int imageDrawable;

    public SampleModel() {
    }

    public String getTitle() {
        return title;
    }

    public SampleModel setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getImage() {
        return image;
    }

    public SampleModel setImage(String image) {
        this.image = image;
        return this;
    }

    public int getImageDrawable() {
        return imageDrawable;
    }

    public SampleModel setImageDrawable(int imageDrawable) {
        this.imageDrawable = imageDrawable;
        return this;
    }
}
