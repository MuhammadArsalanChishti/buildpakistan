package com.build.pakistan.model.dropdown;

/**
 * Created by arsalan.chishti on 9/6/2018.
 */

public class Category {
    private String name;

    public Category(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
