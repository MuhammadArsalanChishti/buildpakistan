package com.build.pakistan.model;

/**
 * Created by arsalan.chishti on 9/3/2018.
 */

public class ListingModel {

    private String name;
    private String company;
    private String category;
    private String image;

    public ListingModel() {
    }

    public String getName() {
        return name;
    }

    public ListingModel setName(String name) {
        this.name = name;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public ListingModel setCompany(String company) {
        this.company = company;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public ListingModel setCategory(String category) {
        this.category = category;
        return this;
    }

    public String getImage() {
        return image;
    }

    public ListingModel setImage(String image) {
        this.image = image;
        return this;
    }
}
