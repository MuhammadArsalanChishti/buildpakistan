package com.build.pakistan.model.dropdown;

/**
 * Created by arsalan.chishti on 9/6/2018.
 */

public class Company {

    private String Company;

    public Company(String company) {
        Company = company;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }
}
