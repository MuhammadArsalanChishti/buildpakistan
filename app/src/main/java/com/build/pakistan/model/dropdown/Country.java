package com.build.pakistan.model.dropdown;

/**
 * Created by arsalan.chishti on 9/6/2018.
 */

public class Country {
    private String name;

    public Country(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
