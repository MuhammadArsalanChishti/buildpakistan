package com.build.pakistan.model;

/**
 * Created by arsalan.chishti on 9/12/2018.
 */

public class RelatedProducts {

    private String name;
    private String companyName;

    public RelatedProducts() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
